<?php


if (filter_input_array(INPUT_POST)) {
    
    $_usuario = trim(filter_input(INPUT_POST, 'usuario'));
    $_pswd    = trim(filter_input(INPUT_POST, 'pswd'));

    include_once '../control/alumnos.php';
    
    $loginok = alumnos::login($_usuario, $_pswd);
   
    if ($loginok){
        header("location: ../index.php?menu=home");
    } else{
       header("location: ../index.php?menu=401");
      
        
    }
}
