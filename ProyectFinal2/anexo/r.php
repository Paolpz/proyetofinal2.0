<?php
    
    $var_getMenu = isset($_GET['menu']) ? $_GET['menu'] : 'inicio';
    

    switch ($var_getMenu) {
        case "inicio":
           require_once('./paginas/portada.php');
           break;

        case "login":
            require_once('./paginas/login.php');
            break;

        case "pt":
            require_once('./paginas/pt.php');
            break;

        case "comida":
            require_once('./paginas/comida.php');
            break;
            
        case "a":
            require_once('./paginas/a.php');
            break;

        case"alumnos":
        include_once ('./control/alumnos.php');
        $sqlAlumnos = alumnos::consultar();
        include_once './paginas/valumnos.php';
        break;

        case "logout":
        $session_destroy = session_destroy();   
        header("location: ./index.php?menu=portada");
        break;

    case "deletealumno":
        $_idalumno = trim(filter_input(INPUT_GET, 'idalumno'));
        require_once ('./control/alumnos.php');        
        $sqlAlumnos = alumnos::delete($_idalumno);
        header("location: ./index.php?menu=alumnos");       
        break;

    case "home":
        require_once('./paginas/home.php');
        break;

    case "401" :
            require_once('./paginas/unauthorized401.php');
            break;

        
        default:
            require_once('./paginas/portada.php');
        } 
    

        ?>

