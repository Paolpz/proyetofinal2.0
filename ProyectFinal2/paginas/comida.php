<?php
$estado_session = session_status();
if ($estado_session == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_SESSION['loggedUserName'])) {
    ?>
    <h3>Bienvenido <?php echo $_SESSION["loggedUserName"]; ?></h3>
    <p>

    <!--cards-->
    <br><br>
        <div class="card-panel  deep-orange">
                <span class="white-text text-darken-2">Conoce mis platillos favoritos</span>
          </div>
      <div class="row">
        <div class="col m4 s12">
          <!--  cambia el color -->
          <div class="card black white-text">
              <div class="card-image">
                  <img src="./imagenes/espagueti_rojo.jpg" alt="" />
                  <div class="card-tittle">Espagueti Rojo</div>
                </div>
            <div class="card-content">
              <p>
                Es una comida que me ha gustado desde que tengo memoria, lo he pedido o preparado en cada uno de mis cumpleaños...incluso lo que preferido antes que un pastel!!!!
              </p>
            </div>
          </div>
        </div>

        <div class="col m4 s12">
          <!--  cambia el color -->
          <div class="card pink darken black-text">
              <div class="card-image">
                  <img src="./imagenes/mole.jpg" alt="" />
                  <div class="card-tittle" >Mole</div>
                </div>
            <div class="card-content">
              <!-- <div class="card-tittle">MI tarjeta</div>-->
              <p>
                El mole me encanta no solo por sus sabores sino tambien representa la union de mi padre y la  mia
              </p>
            </div>
          </div>
        </div>

        <div class="col m4 s12">
          <div class="card blue-grey black-text">
            <div class="card-image">
              <img src="./imagenes/ramen.jpg" alt="" />
              <div class="card-tittle">Ramen</div>
            </div>
            <div class="card-content">
              <p>
                Una comida que me ha dado ilusion provarla dado que era una comida extranjera, el ramen es originari de Japon que he conocido gracias al anime
                gracias a un local en campeche llamado "Koffe Kawaii" he tenido el placer de desgustar este platillo, cumplido y superando las expectativas que tenia.
                De verdad estaba delicioso!!!!
              </p>
            </div>
          </div>
        </div>
      </div>
 <?php }
else{
       header("location: ../index.php?menu=401");
      
    }
  ?>