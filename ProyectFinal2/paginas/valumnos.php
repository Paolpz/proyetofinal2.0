<?php

$estado_session = session_status();
if ($estado_session == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_SESSION['loggedUserName'])) {
    ?>
    <h3>Bienvenido <?php echo $_SESSION["loggedUserName"]; ?></h3>
    <p>

<br>
<h4>Catalogo de Alumnos </h4>
<table class="striped">
    <thead class ="black white-text">
        <tr>
            <th>Matricula</th>
            <th>Alumno</th>
            <th>Sexo</th>
            <th>Simbolo del Sexo</th>

        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($sqlAlumnos as $alumnoview) {
        ?>
            <tr>
                <td><?php echo $alumnoview->alumno; ?></td>
                <td><?php echo $alumnoview->nombre; ?></td>
                <td><?php echo $alumnoview->sexo; ?></td>
                
                
                <td>
                        <?php if ($alumnoview->sexo == 'M') { ?>
                            <i class = "material-icons prefix blue-text">male</i>
                        <?php } else {
                            ?>
                            <i class = "material-icons prefix red-text">female</i>
                        <?php }
                        ?>
                    </td>
                    <td>
                        <button class="btn waves-effect waves-light red" type="submit" name="action">
                            <a href="?menu=deletealumno&idalumno=<?php echo $alumnoview->alumno; ?>"<i class="material-icons right white-text">delete</i></a>
                        </button>                    
                                         
                    </td>

            </tr>
        <?php } ?>
    </tbody>
</table>
<br>

<?php } else{
       header("location: ../index.php?menu=401");
    }?>