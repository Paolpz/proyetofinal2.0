<?php
$estado_session = session_status();
if($estado_session == PHP_SESSION_NONE){
    session_start();
}

if (isset($_SESSION['loggedUserName'])) {

?>

<div class="navbar-fixed">
      <nav>
        <div class="nav-wrapper">
          <a href="#" class="brand-logo right" ><img src="imagenes/UACAM.png" alt="logo" width="50"  height="50"></a>
          <ul id="nav-mobile" class="left hide-on-med-and-down">

         
            <li><a href="?menu=logout"><i class="material-icons">logout</i></a></li>
             <li><a href="?menu=home">Home</a></li>
            <li><a href="?menu=comida">Comida</a></li>
            <li><a href="?menu=a"> Personajes Favoritos</a></li>
            <li><a href="?menu=alumnos">Alumnos</a></li>
             <li><a href="?menu=pt">Acerca de mi...</a></li>

            </ul>
        </div>
    </nav>
</div>
<?php 
} else { ?>

<div class="navbar-fixed">
      <nav>
       
        <div class="nav-wrapper">
          <a href="#" class="brand-logo right" ><img src="imagenes/UACAM.png" alt="logo" width="50"  height="50"></a>
          <ul id="nav-mobile" class="left hide-on-med-and-down">

            <li><a href="?menu=login"><i class="material-icons">account_circle</i>Login</a></li>
            <li><a href="?menu=portada">Inicio</a></li>
          
          </ul>
        </div>
      </nav>
    </div>
    <?php } 

    ?>