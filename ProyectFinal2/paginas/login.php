<?php
$estado_session = session_status();
if($estado_session == PHP_SESSION_NONE)
{
    session_start();
}

if (isset($_SESSION['loggedUserName'])) {
  
}else {    

?>
<br />
<br />

<div class="container">
      <br /><br />
      <div class="row">
        <div class="col s12 m6 offset-m4">
          <form action="./anexo/login.php" method="post">
            <div class="row card-panel z-depth-4">
              <div class="input-field col s12">
                <i class="material-icons prefix">perm_identity</i>
                
                <input
                  type="text"
                  placeholder="Ingresa tu cuenta de usuario"
                  id="usuario"
                  name="usuario"
                  class="validate"
                  required
                />
                <label for="usuario">Usuario:</label>
              </div>
              <div class="input-field col s12">
                <i class="material-icons prefix">lock_open</i>
                <input
                  type="password"
                  id="pswd"
                  name="pswd"
                  class="validate"
                  required
                />
                <label for="pswd">Contraseña:</label>
              </div>
              <button class="btn blue right">
                <i class="material-icons left">login</i>
                Enviar
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <?php }?>