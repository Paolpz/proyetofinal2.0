<?php

//Conexion a BD
//Consulta SQL
include_once 'cdb.php';
class alumnos {

    public $alumno;
    public $nombre;
    public $sexo;

    public function __construct($alumno, $nombre, $sexo) {
        $this->alumno = $alumno;
        $this->nombre = $nombre;
        $this->sexo = $sexo;
    }
    
    public static function consultar() {
        $mysqli = cdb::dbmysql();
        $consulta = "select * from alumnos";
        echo ('<br>');
       
        $resultado = mysqli_query($mysqli, $consulta);
        if (!$resultado) {
            echo 'No pudo Realizar la consulta a la base de datos';
            exit;
        }
        $listaAlumnos = [];
        while ($alumno = mysqli_fetch_array($resultado)) {
            $listaAlumnos[] = new alumnos($alumno['alumno'], $alumno['nombre'], $alumno['sexo']);
        }
        $mysqli->close();
        return $listaAlumnos;
    }

    public static function login($_user, $_password) {
        $mysqli = cdb::dbmysql();

        $stmt = $mysqli->prepare('SELECT user, password  FROM user WHERE user = ? and password = ?');
        $stmt->bind_param('ss', $_user, $_password);
        $stmt->execute();
        $resultado = $stmt->get_result();

        while ($filasql = mysqli_fetch_array($resultado)) {
           
            session_start();
           
            $_SESSION['loggedUserName'] = $filasql['user'] ;
        }

        $acceso = false;
        if ($stmt->affected_rows == 1) {
            $acceso = true;
        }


        $mysqli->close();
        return $acceso;
    }
    
    public static function delete($_idalumno) {
        $mysqli = cdb::dbmysql();
        $stmt = $mysqli->prepare('DELETE FROM alumnos WHERE alumno = ? ');
        $stmt->bind_param('i', $_idalumno);
        $stmt->execute();
        $resultado = $stmt->get_result();
    }
    
    
    
}

?>
